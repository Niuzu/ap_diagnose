/* 
 * File:   AP_DIAG.h
 * Author: MHr
 *
 * Created on 1. August 2019, 21:35
 */

#ifndef AP_DIAGNOSE_H
#define	AP_DIAGNOSE_H


// ##### Including #####
#include "../../global.h"

#include "../../02_kernel/scheduler/task.h"
#include "../../02_kernel/scheduler/SoftwareTimer_SFT.h"

#include "../../03_modul/MD_CAN/MD_CAN.h"

// ##### Task System #####
 extern Task_Status_Byte AP_DIAG_TASK_STATUS;


// ##### Settings #####
#define HARDWARE_VEHICLE 11
#define HARDWARE_VERSION 3

#define SOFTWARE_VEHICLE 12
#define SOFTWARE_VERSION 1

#define NOT_IMPLEMENTED 0
 
#define DIAGNOSE_START_ADDRESS 0x600
 
#define DIAGNOSE_0_ADDRESS DIAGNOSE_START_ADDRESS
#define DIAGNOSE_1_ADDRESS DIAGNOSE_START_ADDRESS + 1
#define DIAGNOSE_2_ADDRESS DIAGNOSE_START_ADDRESS + 2
#define DIAGNOSE_3_ADDRESS DIAGNOSE_START_ADDRESS + 3
 
#define DIAGNOSE_SEND_TIME_MS 1000

// ##### Structs #####
typedef enum{    // (E)lectronic (C)ontroll (U)nit Can status definition
    ECU_NOT_FOUND,          // ECU wasn't found
    ECU_OK,                 // ECU working fine
    ECU_WARNING,            // Values out of boundrys
    ECU_PROGRAMMING,        // ECU in programmer mode via can
    ECU_INIT,               // ECU is powered but still not initialized
    ECU_CYCLE_OVERRUN,      // Task cycle time was over 1ms
    ECU_REBOOT,             // ECU has rebooted
    ECU_ERROR               // ECU has a fatal error
} ECU_Status;


typedef struct {
    uint_least16_t ID;         // ID of the ECU
    uint8_t Hardware_Vehicle;    // Vehicle number of the hardware thats used
    uint8_t Hardware_Version;   // Version of hardware that is used 
    uint8_t Software_Vehicle;   // Vehicle number of the software development
    uint8_t Software_Version;   // Current Software build
    uint16_t Heartbeat;        // increments every can msg to ensure the ECU is still sending
} Diag_0;


typedef struct {
    uint16_t Kernel_Runtime_Min;   // min time the kernel needs for on runthrough
    uint16_t Kernel_Runtime_Max;    // max time the kernel needs for on runthrough
    uint16_t Kernel_Runtime_Avg;   // average time the kernel needs for on runthrough
    uint8_t Software_Timer_Max;     // number of software timers used
    ECU_Status Status : 8;              // current ecu status
} Diag_1;


typedef struct {
    uint64_t not_imp;
} Diag_2;


typedef struct {
    uint64_t not_imp;           
} Diag_3;

// ##### Functions #####
uint_fast8_t AP_Diag_init();
uint_fast8_t AP_Diag_cycle();


#endif	/* AP_DIAG_H */