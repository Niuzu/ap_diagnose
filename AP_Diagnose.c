/* 
 * File:   AP_Diagnose.c
 * Author: MHr
 *
 * Created on 1. August 2019, 21:35
 */


// ##### Including #####
#include "AP_Diagnose.h"


// ##### Task System #####
Task_Status_Byte AP_DIAG_TASK_STATUS;

// ##### Can System ######
CAN_MOB TX_MOB_Diag_0;
CAN_MOB TX_MOB_Diag_1;
CAN_MOB TX_MOB_Diag_2;
CAN_MOB TX_MOB_Diag_3;


// ##### Diag Variables #####
Diag_0 Diag_0_data;
Diag_1 Diag_1_data;
Diag_2 Diag_2_data;
Diag_3 Diag_3_data;


// ##### Timer System #####
static SFT_Timer diag_timer;
static void _diag_timer_event(unsigned_int_short);
static uint_fast8_t current_mob_to_send;


// ##### prototypes #####
void _update_data(void);
    

// ##### functions #####
/*
 * Init diag and fill can mob
 */
uint_fast8_t AP_Diag_init(){
    // ######## Init Diagnoes structs
    // Diag 0
    Diag_0_data.ID = 1;
    Diag_0_data.Hardware_Vehicle = HARDWARE_VEHICLE;
    Diag_0_data.Hardware_Version = HARDWARE_VERSION;
    Diag_0_data.Software_Vehicle = SOFTWARE_VEHICLE;
    Diag_0_data.Software_Version = SOFTWARE_VERSION;
    Diag_0_data.Heartbeat = 0;
    
    // Diag 1
    Diag_1_data.Kernel_Runtime_Min = NOT_IMPLEMENTED;
    Diag_1_data.Kernel_Runtime_Max = NOT_IMPLEMENTED;
    Diag_1_data.Kernel_Runtime_Avg = NOT_IMPLEMENTED;
    Diag_1_data.Software_Timer_Max = NOT_IMPLEMENTED;
    Diag_1_data.Status = ECU_INIT;
    
    // Diag 2
    Diag_2_data.not_imp = NOT_IMPLEMENTED;
    
    // Diag 3
    Diag_3_data.not_imp = NOT_IMPLEMENTED;
    
    
    // ######## Init Can Mobs
      // construct CAN MSG Diag 0
    TX_MOB_Diag_0.Command = CMD_TX_DATA;                   // Set MSG-command to TransmitData
    TX_MOB_Diag_0.Hardware_buffer = 0;                     // Set MSG-hardware
    TX_MOB_Diag_0.Identifyer.standard = DIAGNOSE_0_ADDRESS;   // Set MSG-identifyer to standard
    TX_MOB_Diag_0.data_length = 8;                         // Set MSG-dataLength to 8 byte
    TX_MOB_Diag_0.frame_type = standard;                   // Set MSG-frameType to standard
    
    // construct CAN MSG Diag 1
    TX_MOB_Diag_1.Command = CMD_TX_DATA;                   // Set MSG-command to TransmitData
    TX_MOB_Diag_1.Hardware_buffer = 0;                     // Set MSG-hardware
    TX_MOB_Diag_1.Identifyer.standard = DIAGNOSE_1_ADDRESS;   // Set MSG-identifyer to standard
    TX_MOB_Diag_1.data_length = 8;                         // Set MSG-dataLength to 8 byte
    TX_MOB_Diag_1.frame_type = standard;                   // Set MSG-frameType to standard
    
    // construct CAN MSG Diag 2
    TX_MOB_Diag_2.Command = CMD_TX_DATA;                   // Set MSG-command to TransmitData
    TX_MOB_Diag_2.Hardware_buffer = 0;                     // Set MSG-hardware
    TX_MOB_Diag_2.Identifyer.standard = DIAGNOSE_2_ADDRESS;   // Set MSG-identifyer to standard
    TX_MOB_Diag_2.data_length = 8;                         // Set MSG-dataLength to 8 byte
    TX_MOB_Diag_2.frame_type = standard;                   // Set MSG-frameType to standard
    
    // construct CAN MSG Diag 3
    TX_MOB_Diag_3.Command = CMD_TX_DATA;                   // Set MSG-command to TransmitData
    TX_MOB_Diag_3.Hardware_buffer = 0;                     // Set MSG-hardware
    TX_MOB_Diag_3.Identifyer.standard = DIAGNOSE_3_ADDRESS;   // Set MSG-identifyer to standard
    TX_MOB_Diag_3.data_length = 8;                         // Set MSG-dataLength to 8 byte
    TX_MOB_Diag_3.frame_type = standard;                   // Set MSG-frameType to standard
    
    // Set diagnose timer
    diag_timer.time_ms = DIAGNOSE_SEND_TIME_MS / 4;
    diag_timer.time_out_function = &_diag_timer_event;
    
    // Start Timer
    SFT_start(&diag_timer);
    
    // Set Task to Idle
    AP_DIAG_TASK_STATUS.TaskStatus = IDLE;

    return OK;
}


/*
 * 
 */
uint_fast8_t AP_Diag_cycle(){
        switch(AP_DIAG_TASK_STATUS.TaskStatus) {
            case STOP:
            break;
            
            
            case PAUSE:
            break;
            
            
            case IDLE:
            break;
            
            
            case RUN:
                _update_data();
            break;
            
            
            case INIT:
            break;
            
            
            default:
            break;
                
        }
        
    return OK;
}

/*
 * Update data from Diag structs to can mobs
 */
void _update_data(void){
    memcpy(TX_MOB_Diag_0.data, &Diag_0_data, 8);
    memcpy(TX_MOB_Diag_1.data, &Diag_1_data, 8);
    memcpy(TX_MOB_Diag_2.data, &Diag_2_data, 8);
    memcpy(TX_MOB_Diag_3.data, &Diag_3_data, 8);
    
    AP_DIAG_TASK_STATUS.TaskStatus = IDLE;
}

/*
 * Gets called everytime the timer expires, pushes one diag can msg at a time
 * Msg's are split because the can msq souldn't get to much traffic at once
 */
static void _diag_timer_event(unsigned_int_short var){
    switch(current_mob_to_send){
        case 0:
            MSQ_message_push(&MD_CAN_MSQ_Queue, &TX_MOB_Diag_0);
            current_mob_to_send++;
            break;
            
        case 1:
            MSQ_message_push(&MD_CAN_MSQ_Queue, &TX_MOB_Diag_1);
            current_mob_to_send++;
            break;
            
        case 2:
            MSQ_message_push(&MD_CAN_MSQ_Queue, &TX_MOB_Diag_2);
            current_mob_to_send++;
            break;
            
        case 3:
            MSQ_message_push(&MD_CAN_MSQ_Queue, &TX_MOB_Diag_3);
            current_mob_to_send = 0;
            break;
            
        default:
            current_mob_to_send = 0;
            break;
    }
    AP_DIAG_TASK_STATUS.TaskStatus = IDLE;
}
